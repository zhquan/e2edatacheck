End to end data check for GrimoireLab indexes.

Looking for help?
```
usage: e2edatacheck [-h] [--human]
                    projects_file endpoint_raw index_raw endpoint_enriched
                    index_enriched ds

e2edatacheck, the end to end data check for GrimoireLab indexes

positional arguments:
  projects_file      file path to projects.json file
  endpoint_raw       ES endpoint where raw index is stored
  index_raw          name of the raw index
  endpoint_enriched  ES endpoint where enriched index is stored
  index_enriched     name of the enriched index
  ds                 data source (git, github, gitlab:issue, gitlab:merge)

optional arguments:
  -h, --help         show this help message and exit
  --human            Human readable output

Software metrics for your peace of mind
```


Looking for examples?
```
$ e2edatacheck /tmp/projects.json https://bitergia:***@es1.bitergia.net/data github_raw https://bitergia:****@es2.bitergia.net/data github_enriched github 
{
    "datasource": "Github",
    "diff": {
        "closed": 0,
        "open": -1,
        "total": -1
    },
    "enriched": {
        "closed": 249,
        "open": 32,
        "total": 281
    },
    "origin": "https://github.com/kata-containers/osbuilder",
    "raw": {
        "closed": 249,
        "open": 32,
        "total": 281
    },
    "timestamp": 1556232356.793069,
    "upstream": {
        "closed": 249,
        "open": 33,
        "total": 282
    }
}
..

$ e2edatacheck /tmp/projects.json https://bitergia:***@es1.bitergia.net/data github_raw https://bitergia:****@es2.bitergia.net/data github_enriched github --human
OK (Github) https://github.com/kata-containers/agent
OK (Github) https://github.com/kata-containers/proxy
Warning: (Github) https://github.com/kata-containers/documentation
+----------+--------+------+-------+
|          | closed | open | total |
+----------+--------+------+-------+
| Upstream |  369   |  77  |  446  |
|   Raw    |  369   |  77  |  446  |
| Enriched |  369   |  76  |  445  |
|   Diff   |   0    |  -1  |   -1  |
+----------+--------+------+-------+
Warning: (Github) https://github.com/kata-containers/osbuilder
+----------+--------+------+-------+
|          | closed | open | total |
+----------+--------+------+-------+
| Upstream |  249   |  33  |  282  |
|   Raw    |  249   |  32  |  281  |
| Enriched |  249   |  32  |  281  |
|   Diff   |   0    |  -1  |   -1  |
+----------+--------+------+-------+

```
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#

import requests
import urllib.parse

from bs4 import BeautifulSoup


def items_per_repo(repo_url):
    """Returns number of items per state found in the web interface of Github issues.
    Example: https://github.com/mozilla/participation-metrics-org/issues?q=

    :param repo_url: URL of the repo to be queried
    :return: list of integers
    """

    def _extract_number(mystring):
        return int(mystring.strip().split(' ')[0].replace(',',''))

    repo_url = repo_url + '/'
    url = urllib.parse.urljoin(repo_url, "./issues?q=")
    r = requests.get(url)
    if r.status_code != 200: return -1

    soup = BeautifulSoup(r.text,'html.parser')
    pills = [i.get_text() for i in soup.find_all("a", "btn-link")]
    int_pills = [_extract_number(j) for j in pills]
    return int_pills


